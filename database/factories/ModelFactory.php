<?php

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => Hash::make('123'),
        'type' => 'user',
        'gender' => 'male',
        'phone_number' => $faker->phoneNumber
    ];
});


$factory->state(App\User::class, 'provider', function ($faker) {
    return [
        'type' => 'provider',
    ];
});


$factory->define(App\Community::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'user_id' => factory('App\User')->create()->id
    ];
});
$factory->define(App\Challenge::class, function (Faker\Generator $faker) {
    return [
        'challengable_id' => factory('App\User')->create()->id,
        'challengable_type' => 'App\User',
        'gov_id' => factory('App\Governorate')->create()->id,
        'activity_id' => factory('App\Activity')->create()->id,
        'require_confirmation' => true,
        'challenge_at' => Carbon::now()
    ];
});



$factory->define(App\Activity::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->title,
        'icon' => 'icon'
    ];
});

$factory->define(App\Governorate::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->title
    ];
});

$factory->define(App\Court::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->title,
        'activity_id' => factory('App\Activity')->create()->id,
        'user_id' => factory('App\User')->create()->id,
        'price' => $faker->randomNumber()
    ];
});

$factory->define(App\Scheduler::class, function (Faker\Generator $faker) {
    return [
        'court_id' => factory('App\Court')->create()->id,
        'user_id' => factory('App\User')->create()->id,
        'receipt_id' => Str::random(),
        'started_at' => $faker->dateTime(),
        'ended_at' => $faker->dateTime()
    ];


});

