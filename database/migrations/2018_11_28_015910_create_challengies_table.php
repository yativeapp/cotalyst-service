<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Challenge;

class CreateChallengiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->morphs('challenger');
            $table->timestamp('challenge_at')->nullable();
            $table->unsignedInteger('activity_id');
            $table->unsignedInteger('provider_id')->nullable();
            $table->unsignedInteger('gov_id');
            $table->unsignedInteger('winner_id')->nullable();
            $table->string('status')->default(Challenge::WAITING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}
