<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use App\Activity;
use App\Role;
use App\Governorate;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();

        $admin = factory('App\User')->create([
            'type' => 'admin',
            'email' => 'admin@cotalysty',
            'password' => Hash::make('123')
        ]);

        $admin->roles()->associate(
            Role::where('name', 'admin')->first()
        );

        factory('App\User')->create([
            'type' => 'user',
            'email' => 'user@gmail.com',
            'password' => Hash::make('12345678')
        ]);

        // factory('App\User')->create([
        //     'type' => 'user',
        //     'email' => 'user2',
        //     'password' => Hash::make('123')
        // ]);


        // $provider = factory('App\User')->create([
        //     'type' => 'provider',
        //     'email' => 'provider',
        //     'password' => Hash::make('123')
        // ]);

        // $provider->gov()->save(
        //     Governorate::first()
        // );


        // factory('App\Court', 5)->create([
        //     'user_id' => $provider->id,
        //     'activity_id' => Activity::first()->id
        // ]);
    }
}
