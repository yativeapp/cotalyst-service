<?php

use Illuminate\Database\Seeder;

use App\Governorate;

class GovernoratesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $govs = [
            [
                'name' => 'maadi'
            ],
            [
                'name' => 'dooki'
            ],
            [
                'name' => 'Alex'
            ],
            [
                'name' => 'Giza'
            ]
        ];

        Governorate::truncate();

        Governorate::insert($govs);
    }
}
