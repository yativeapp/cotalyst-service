<?php

use Illuminate\Database\Seeder;
use App\Activity;

class ActivityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activity::truncate();

        Activity::create([
            'name' => 'Football',
            'icon' => 'https://firebasestorage.googleapis.com/v0/b/catalysty-269e2.appspot.com/o/football.png?alt=media&token=0cc6f3fb-690e-4582-9142-b71b5c665e63'
        ]);
        Activity::create([
            'name' => 'PaintBall',
            'icon' => 'https://firebasestorage.googleapis.com/v0/b/catalysty-269e2.appspot.com/o/paintball.png?alt=media&token=3ba00070-7519-418c-b2e5-28792eb040b2'
        ]);
        Activity::create([
            'name' => 'Ping Pong',
            'icon' => 'https://firebasestorage.googleapis.com/v0/b/catalysty-269e2.appspot.com/o/icons8-ping-pong-48.png?alt=media&token=468aec8b-7d23-45ad-b053-c5e74776616e'
        ]);
        Activity::create([
            'name' => 'Tennis',
            'icon' => 'https://firebasestorage.googleapis.com/v0/b/catalysty-269e2.appspot.com/o/icons8-tennis-racquet-48.png?alt=media&token=a80be7ce-a8ca-43b1-8e07-ef426f7c0e44'
        ]);
    }
}
