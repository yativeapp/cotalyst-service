<?php

use Illuminate\Database\Seeder;
use App\Activity;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call('RolesTableSeeder');
        // $this->call('ActivityTableSeeder');
        $this->call('GovernoratesTableSeeder');
        $this->call('UsersTableSeeder');


    }
}
