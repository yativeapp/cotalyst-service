<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Activity;

class CourtTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function it_adds_new_court_to_activity()
     {
          $activity = factory('App\Activity')->create();

          $this->post("/courts/store", [
               'name' => 'court 1',
               'activity_id' => $activity->id,
               'user_id' => 1
          ]);

          $this->assertCount(1, $activity->courts);

          $this->assertEquals('court 1', $activity->courts->first()->name);
     }
}