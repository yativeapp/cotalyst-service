<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Faker\Provider\DateTime;
use Carbon\Carbon;

class MatchTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function user_can_match_a_challenge()
     {

          //assume we have a user
          Passport::actingAs(
               $firstUser = factory('App\User')->create()
          );
          //and other user posted a challenge
          $secondUser = factory('App\User')->create();

          $challenge = factory('App\Challenge')->create([
               'challengable_id' => $secondUser->id
          ]);
          //when first user match the challenge

          $this->post("/challenge/$challenge->id/match");


          $this->assertTrue(
               $challenge->match->matchable->is($firstUser)
          );
     }

     public function user_should_be_notified_onec_someone_match_his_challenge()
     {
           
           //user who posted the challenge should be notified

          //and first user should have one challenge now
     }
}