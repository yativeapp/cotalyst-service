<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Activity;

class ActivityTest extends TestCase
{
    // use DatabaseTransactions;

    /** @test */
    public function it_saves_the_activity_upon_request()
    {
        $this->post('/activities', ['name' => 'foot']);

        $this->assertCount(1, Activity::all());

        $this->assertEquals('foot', Activity::first()->name);
    }

    /** @test */
    public function activity_can_be_assigned_to_a_provider()
    {
        $provider = factory('App\User')->state('provider')->create();

        $activities = factory('App\Activity', 3)->create();

        $this->post('/activities/assign', [
            'provider' => $provider->id,
            'activities' => $activities->pluck('id')->toArray()
        ]);

        $this->assertCount(
            3,
            $provider->activities->intersect($activities)
        );

    }
}
