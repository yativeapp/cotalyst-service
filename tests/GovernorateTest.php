<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Governorate;

class GovernorateTest extends TestCase
{
     use DatabaseMigrations;


     /** @test */
     public function can_add_new_governorate()
     {

          $this->post('/governorates', ['name' => 'dooki']);

          $this->assertEquals('dooki', Governorate::first()->name);
     }
}

