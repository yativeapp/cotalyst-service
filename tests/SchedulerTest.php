<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Activity;
use App\User;
use Carbon\Carbon;

class SchedulerTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function user_can_book_a_court_in_some_activity()
     {
          $court = factory('App\Court')->create();

          $bookedAt = Carbon::now();

          $this->post("/courts/$court->id/schedule", [
               'booked_at' => $bookedAt
          ]);

          $this->assertCount(1, $court->schedules);

          $this->assertTrue($bookedAt->eq($court->schedules->first()->booked_at));
     }

     /** @test */
     public function ca_get_all_bookes_in_a_certain_date()
     {
          $bookedAt = Carbon::now();

          $schedule = factory('App\Scheduler')->create(compact('booked_at'));


          $res = $this->json('GET', '/schedules', [
               'date' => $bookedAt->toDateString(),
               'time' => $bookedAt->toTimeString()
          ])->seeJson($schedule->toArray());

     }

}