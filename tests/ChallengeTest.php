<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Faker\Provider\DateTime;
use Carbon\Carbon;

class ChallengeTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function user_can_create_new_challenge()
     {
          Passport::actingAs(
               $user = factory('App\User')->create()
          );

          $this->post('/challenge/store', [
               'gov_id' => 1,
               'activity_id' => 1,
               'require_confirmation' => true,
               'challenge_at' => Carbon::now()
          ]);

          $this->assertCount(1, $user->challenge()->get());

     }

     /** @test */
     public function user_can_get_his_own_challengies()
     {
          Passport::actingAs(
               $user = factory('App\User')->create()
          );

          $challenge = factory('App\Challenge')->create([
               'challengable_id' => $user->id
          ]);

          $this->get('/challenges')->seeJson($challenge->toArray());
     }

     /** @test */
     public function user_can_not_create_new_challenge_for_the_same_date()
     {
          Passport::actingAs(
               $user = factory('App\User')->create()
          );

          $date = Carbon::now();

          $this->post('/challenge/store', [
               'gov_id' => 1,
               'activity_id' => 1,
               'require_confirmation' => true,
               'challenge_at' => Carbon::now()
          ]);


          $this->post('/challenge/store', [
               'gov_id' => 1,
               'activity_id' => 1,
               'require_confirmation' => true,
               'challenge_at' => Carbon::now()
          ]);


          $this->assertCount(1, $user->challenge()->get());
     }

     /** @test */
     public function user_can_create_new_challenge_as_a_community()
     {
          Passport::actingAs(
               $user = factory('App\User')->create()
          );

          $community = factory('App\Community')->create(['user_id' => $user->id]);

          $this->post('/challenge/store', [
               'community_id' => $community->id,
               'gov_id' => 1,
               'activity_id' => 1,
               'require_confirmation' => true,
               'challenge_at' => Carbon::now()
          ]);

          $this->assertCount(1, $community->challenge()->get());

     }
}