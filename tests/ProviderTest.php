<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Activity;
use App\User;

class ProviderTest extends TestCase
{
     use DatabaseMigrations;

     /** @test */
     public function it_can_get_all_providers()
     {

          $providers = factory('App\User', 3)->state('provider')->create();


          $res = $this->call('GET', 'providers');

        
          // dd($res->seeJson($providers->toArray()));
     }

     /** @test */
     public function it_creates_new_provider()
     {
          $gov = factory('App\Governorate')->create();

          $this->post('/providers', [
               'email' => 'provider@gmail.com',
               'password' => 'provider',
               'name' => 'provider-one',
               'gov' => $gov->id
          ]);

          $user = User::with('place')->where([
               'email' => 'provider@gmail.com',
               'name' => 'provider-one'
          ])->get();
          $this->assertCount(1, $user);

          $this->assertTrue($gov->is($user->place));
     }


}
