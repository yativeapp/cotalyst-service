<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scheduler;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Challenge extends Model
{

     const REQUESTED = 'requested';

     const WAITING = 'waiting';

     const STARTED = 'started';

     const FINISHED = 'finished';


     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'name', 'activity_id', 'gov_id', 'provider_id', 'winner_id', 'challenge_at', 'status'
     ];

     protected $dates = ['challenge_at'];


     protected $appends = ['ismychallenge', 'canshow', 'iswinner', 'canscore'];

     protected $with = ['matches', 'challenger', 'activity', 'gov'];


     protected static function boot()
     {
          parent::boot();

          static::creating(function ($model) {

               $model->name = 'Challenge #' . (static::count() + 1);
          });
     }


     public function getCanShowAttribute()
     {

          if (Auth::guest()) return false;

          $authId = Auth::id();

    
          if ($this->challenger_type == Community::class) {

               return $this->challenger->users->pluck('id')->contains($authId) ||
                    $this->challenger->admin->id == $authId ||
                    $this->matches->whereIn(
                    'matchable_id',
                    Auth::user()->communities->pluck('id')->toArray()
               )->count();
          }

          return $this->challenger->id == $authId || $this->matches->whereIn(
               'matchable_id',
               Auth::id()
          )->count();
     }
     public function getIsMyChallengeAttribute()
     {

          if (Auth::guest()) return false;


          $authId = Auth::id();


          if ($this->challenger_type == Community::class) {

               return $this->challenger->users->pluck('id')->contains($authId) ||
                    $this->challenger->admin->id == $authId;
          }

          return $this->challenger->id == $authId;
     }


     public function getIsWinnerAttribute()
     {

          $authId = Auth::id();

          if (!$authId || !$this->winner_id) return false;

          if ($this->challenger_type == Community::class) {

               $isMyCommunity = $this->challenger->users->pluck('id')->contains($authId);

               if ($isMyCommunity && $this->winner_id == $this->challenger_id) {

                    return true;
               }

               if (!$isMyCommunity && $this->winner_id != $this->challenger_id) {

                    return true;
               }

               if ($isMyCommunity && $this->winner_id != $this->challenger_id) {

                    return false;
               }
          }

          return $this->winner_id == $authId;
     }


     public function getCanScoreAttribute()
     {

          if ($this->relationLoaded('schedules')) {


               if ($this->schedules->count() > 0) {

                    return $this->schedules->last()->ended_at->lt(Carbon::now()->subMinutes(30));
               }

          }


          return false;

     }

     public function challenger()
     {

          return $this->morphTo();
     }


     public function activity()
     {

          return $this->belongsTo(Activity::class);
     }

     public function gov()
     {

          return $this->belongsTo(Governorate::class);
     }


     public function matches()
     {

          return $this->hasMany(Match::class);
     }

     public function schedules()
     {

          return $this->hasMany(Scheduler::class, 'challenge_id');
     }

     public function provider()
     {

          return $this->hasOne(User::class, 'id', 'provider_id');
     }


}