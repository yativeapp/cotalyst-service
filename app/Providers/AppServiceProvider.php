<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Extras\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {


        Passport::routes($this->app);
    }
}
