<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Community extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'user_id', 'score', 'name'
     ];

     protected $with = ['admin', 'users'];

     protected $appends = ['is_admin', 'is_member', 'requested', 'rate'];

     /**
      * The attributes excluded from the model's JSON form.
      *
      * @var array
      */
     protected $hidden = [
          'score'
     ];

     public function getIsAdminAttribute()
     {

          if (Auth::guest()) return false;

          return Auth::user()->id == $this->user_id;
     }

     public function getIsMemberAttribute()
     {

          if(Auth::guest()) return false;

          if ($this->is_admin) return true;

          return $this->users->contains(function ($user) {
               return $user->id == Auth::user()->id;
          });
     }

     public function getRequestedAttribute()
     {
          if(Auth::guest()) return false;

          return $this->users->contains(function ($user) {
               return $user->id == Auth::user()->id && $user->accepted == false;
          });
     }

     public function getRateAttribute($attr)
     {


          return $this->score->avg('points') ?? 0;
     }


     public function tokens($withAdmin = true)
     {

          return $this->users->push($this->admin)->filter(function ($user) {
               return $user->id != Auth::user()->id;
          })->pluck('token')->toArray();

     }
     public function admin()
     {

          return $this->belongsTo(User::class, 'user_id');
     }

     public function challenges()
     {
          return $this->morphMany(Challenge::class, 'challenger');
     }

     public function matches()
     {

          return $this->morphMany(Match::class, 'matchable');
     }

     public function users()
     {

          return $this->belongsToMany(User::class)->latest()->withPivot('accepted');
     }

     public function score()
     {


          return $this->morphMany(Score::class, 'scorable');
     }


}
