<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Inqury extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'content', 'reply' , 'resolved' , 'user_id'
     ];


     public function user()
     {
           
          return $this->belongsTo(User::class);
     }
}