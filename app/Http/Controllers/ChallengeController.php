<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use App\Challenge;
use Carbon\Carbon;
use App\Community;
use App\Extras\FCMProvider;
use Auth;

class ChallengeController extends Controller
{

     public function index(Request $request)
     {


          $challengies = Challenge::with('challenger');


          if ($request->filled('activity')) {
               $challengies->where('activity_id', $request->input('activity'));
          }

          if ($request->filled('gov')) {
               $challengies->where('gov_id', $request->input('gov'));
          }

          if ($request->filled('status') && $request->input('status') != 'all') {
               $challengies->where('status', $request->input('status'));
          }



          if ($request->filled('dateRange')) {

               $challengies->whereDate('created_at', '>', $request->input('dateRange.0'))
                    ->whereDate('created_at', '<', $request->input('dateRange.1'));
          }
          if ($request->filled('challenge_at')) {

               $challengies->whereDate(
                    'challenge_at', 
                    Carbon::parse($request->input('challenge_at'))
               );
          }

   

          if ($request->filled('limit')) {
                $challengies->paginate($request->input('limit'));
          }

          if ($request->filled('searchKey')) {
               $challengies->where('name' , 'like' , '%'.$request->input('searchKey').'%');
          }

          if ($request->filled('app')) {

               return $challengies->get()->filter(function ($challenge) {
                    return !$challenge->canshow;
               })->values();
          }

          if($request->filled('user_id')){

               $challengies->where([
                    'challenger_type' => User::class,
                    'challenger_id' => $request->input('user_id')
               ]);

               
          }

          return $challengies->orderBy('created_at')->paginate($request->input('limit'));
     }
     
     public function total()
     {
           
          return Challenge::count();
     }

     public function show($id)
     {

          return Challenge::with(['schedules','provider', 'challenger', 'matches'])->findOrFail($id);
     }

     public function matches($id)
     {

          return Challenge::findOrFail($id)->matches()->with('matched')->get();
     }

     public function schedules($id)
     {
          return Challenge::find($id)->schedules()->get();
     }
     public function user(Request $request)
     {
          $challenges = $request->user()->challenges;

          $communities_challenges = $request->user()->communities()->with('challenges')
               ->get()->map->challenges->first();


          return $challenges->merge($communities_challenges ?? []);
     }



     public function store(Request $request, FCMProvider $fcm)
     {
          $this->validate($request, [
               'activity_id' => 'required',
               'gov_id' => 'required'
          ]);

          $entity = $request->input('community_id') != 0 ? Community::find($request->input('community_id')) : $request->user();

          $timestamp = null;

          if ($request->filled('challenge_at')) {
               $timestamp = Carbon::parse($request->input('challenge_at'));

          }

          $challenge = $entity->challenges()->create([
               'challenge_at' => $timestamp ? $timestamp->toDateTimeString() : null,
               'provider_id' => $request->input('provider_id'),
               'activity_id' => $request->input('activity_id'),
               'gov_id' => $request->input('gov_id'),
          ]);

          if ($request->input('community_id') != 0) {

               $tokens = $entity->tokens();

               if(sizeof($tokens) > 0){
 $fcm->title('New Challenge For ' . $entity->name)
                    ->body($challenge->challenger->name . ' Started a New Challenge At ' . $entity->name . ' Community')
                    ->data(['challenge_id' => $challenge->id, 'for' => 'community', 'comm_id' => $entity->id], "community-new-challenge")
                    ->send($tokens);
               }
              
          }

          return $challenge->load(['activity', 'gov', 'challenger']);

     }

     public function destroy(Request $request, FCMProvider $fcm)
     {
          $this->validate($request, [
               'challenge_id' => 'required'
          ]);

        $challenge = Challenge::findOrFail($request->input('challenge_id'));

        $challenge->schedules()->destroy();

        $challenge->matches()->destroy();

        $challenge->destroy();


         if ($request->filled('community_id')) {

          //self members notify
               $for = 'community';

               $fcm->title('Challenge Match Canceled')
                    ->body(Auth::user()->name . ' Cancel Challenge Match in ' . $challenge->challenger->name . ' From ' . $match->matchable->name)
                    ->data(['challenge_id' => $challenge->id, 'match_id' => $match->id, 'for' => $for, 'comm_id' => $challenge->challenger->id], 'match-canceled')
                    ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();

          } else {

               $for = 'user';

               $tokens = [$match->matchable->token];
          }

          $fcm->title('Challenge Match Canceled')
               ->body($challenge->challenger->name . ' Canceled His Challenge Match')
               ->data(['challenge_id' => $challenge->id, 'match_id' => $match->id, 'for' => $for, 'comm_id' => $match->matchable->id], 'match-canceled')
               ->send($tokens);


     }

     public function accept(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'challengeId' => 'required',
               'matchId' => 'required',
          ]);


          $challenge = Challenge::with('challenger')->findOrFail($request->input('challengeId'));

          $challenge->update([
               'status' => Challenge::STARTED
          ]);

          //notify other
          $match = $challenge->matches()->with('matched')->where('id', $request->input('matchId'))->first();

          $match->update([
               'selected' => true
          ]);

          $comm_id = 0;

          if ($match->matchable_type == Community::class) {

               $tokens = $match->matchable->tokens();

               $for = 'community';

               $comm_id = $match->matchable->id;

          } else if ($match->matchable_type == User::class) {

               $for = 'user';

               $tokens = [$match->matchable->token];
          }


          $fcm->title('Challenge Started')
               ->body('Your Challenge With ' . $challenge->challenger->name . ' Has Been Started')
               ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $comm_id], 'match-accepted')
               ->send($tokens);


     }

     public function cancel(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'challengeId' => 'required'
          ]);

          $challenge = Challenge::with('matches.matched')->find($request->input('challengeId'));

          $match = $challenge->matches->first();


          $challenge->delete();

          $comm_id = 0;

          if ($match->matchable_type == Community::class) {

               $tokens = $match->matchable->tokens();

               $for = 'community';

               $comm_id = $match->matchable->id;

          } else if ($match->matchable_type == User::class) {

               $for = 'user';

               $tokens = [$match->matchable->token];
          }


          $fcm->title('Challenge Request Canceled')
               ->body($challenge->challenger->name . 'Canceled Your Challenge Request')
               ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $comm_id], 'challange-canceled')
               ->send($tokens);
     }

     public function changeTimeStamp(Request $request , FCMProvider $fcm)
     {


          $this->validate($request, [
               'challengeId' => 'required',
               'timestamp' => 'required',
          ]);

          $challenge = Challenge::findOrFail($request->input('challengeId'));

          $date = $request->input('timestamp');

          $challenge->update([
               'challenge_at' => Carbon::parse($date)
          ]);

          $match = $challenge->matches->where('selected' , true)->first();


          $comm_id = 0;

          if($challenge->challenger_type == Community::class){



                //self members notify
               $for = 'community';

           
          $fcm->title('Challenge Date updated')
               ->body($request->user()->name . ' Updated Date For' . $challenge->name . ' Challenge')
               ->data(['challenge_id' => $challenge->id, 'date' => $date, 'for' => $for, 'comm_id' => $comm_id], 'challenge-date-changed')
               ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();


          }else{

               $for = 'user';

            

       $other = collect([$match->matchable->id , $challenge->challenger_id])->filter(function($id) use ($request){
                   return $id != $request->user()->id;
               })->map(function($id){
return User::find($id);
               })->first();

               $tokens = [$other->token];

          }


          $fcm->title('Challenge Date updated')
               ->body($request->user()->name . ' Updated Date For' . $challenge->name . ' Challenge')
               ->data(['challenge_id' => $challenge->id, 'date' => $date, 'for' => $for, 'comm_id' => $comm_id], 'challenge-date-changed')
               ->send($tokens);


     }
     public function changeName(Request $request , FCMProvider $fcm)
     {


          $this->validate($request, [
               'challenge_id' => 'required',
               'name' => 'required',
          ]);

          $challenge = Challenge::with('challenger')->findOrFail($request->input('challenge_id'));

          $oldName = $challenge->name;

          $newName = $request->input('name');

          $challenge->update([
               'name' => $newName
          ]);




                 //notify other
          $match = $challenge->matches()->where('selected' , true)->with('matched')->first();

    
          if($match){
 $comm_id = 0;

          if ($match->matchable_type == Community::class) {


               //self members notify
               $for = 'community';

               $fcm->title('Challenge Name Changed')
                    ->body(Auth::user()->name . ' Changed Challenge Name From  ' . $oldName . ' To ' . $newName)
                    ->data(['challenge_id' => $challenge->id, 'name' =>$newName , 'for' => $for, 'comm_id' => $challenge->challenger->id], 'challenge-name-changed')
                    ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();

          } else if ($match->matchable_type == User::class) {

               $for = 'user';

               $tokens = [$match->matchable->token];
          }


                    $fcm->title('Challenge Name Changed')
                    ->body(Auth::user()->name . ' Changed Challenge Name From  ' . $oldName . ' To ' . $newName)
               ->data(['challenge_id' => $challenge->id,'name' =>$newName, 'for' => $for, 'comm_id' => $comm_id], 'challenge-name-changed')
               ->send($tokens);
          }
         

     }




     public function getRequests(Request $request)
     {

          $id = $request->user()->id;

          $c1 = Challenge::with('matches')->where([
               'challenger_type' => User::class,
               'challenger_id' => $id,

          ])->where('status', Challenge::REQUESTED)->get();

          $c2 = Challenge::with('matches')->whereIn('status', [Challenge::REQUESTED, Challenge::WAITING])->whereHas('matches', function ($q) use ($id) {
               return $q->where([
                    'matchable_type' => User::class,
                    'matchable_id' => $id
               ]);
          })->get();

          return $c1->merge($c2)->sortByDesc('created_at')->values();
     }

     public function getActive(Request $request)
     {

          $id = $request->user()->id;

          $c1 = Challenge::where([
               'challenger_type' => User::class,
               'challenger_id' => $id,

          ])->whereIn('status', [Challenge::STARTED, Challenge::WAITING])->get();

          $c2 = Challenge::where('status', Challenge::STARTED)->whereHas('matches', function ($q) use ($id) {
               return $q->where([
                    'matchable_type' => User::class,
                    'matchable_id' => $id
               ]);
          })->get();

            return $c1->merge($c2)->sortByDesc('created_at')->values();
     }

     public function getHistory(Request $request)
     {


          $id = $request->user()->id;

          $c1 = Challenge::where([
               'challenger_type' => User::class,
               'challenger_id' => $id,

          ])->where('status', Challenge::FINISHED)->get();

          $c2 = Challenge::where('status', Challenge::FINISHED)->whereHas('matches', function ($q) use ($id) {
               return $q->where([
                    'matchable_type' => User::class,
                    'matchable_id' => $id,
                    'selected' => true
               ]);
          })->get();

            return $c1->merge($c2)->sortByDesc('created_at')->values();
     }
}