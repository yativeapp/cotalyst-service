<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Governorate;
use Illuminate\Support\Str;


class HomeController extends Controller
{

     public function index()
     {

          return view('admin.index');
     }

     public function login(Request $request)
     {

          $this->validate($request, [
               'username' => 'required',
               'password' => 'required'
          ]);

          $user = User::whereType('admin')->whereEmail($request->input('username'))->first();


          if (!$user) return response('Wrong Creds', 500);

          if ((Hash::check($request->input('password'), $user->password))) {
               $token = $user->createToken('cotalysty')->accessToken;

               return response()->json(['data' => ['token' => $token]]);
          } else {
               return response('Wrong Creds', 500);
          }
     }

     public function dashboard()
     {
          $governorates = Governorate::get();

          return view('admin.index', compact('governorates'));
     }

}