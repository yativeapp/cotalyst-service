<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use App\Challenge;

use App\Community;
use App\Invite;
use App\Extras\FCMProvider;


class InviteController extends Controller
{

     public function index(Request $request)
     {

          return $request->user()->invites()->with('community')->get();
     }

     public function store(Request $request, FCMProvider $fcm)
     {
          $this->validate($request, [
               'community_id' => 'required',
               'user_id' => 'required'
          ]);

          $invite = Invite::create([
               'community_id' => $request->input('community_id'),
               'user_id' => $request->input('user_id')
          ]);

          $invite->load('community');

          $user = User::find($request->input('user_id'));



          $fcm->title('You Have an Invite')
               ->body($request->user()->name, 'Invited You to' . $invite->community->name . 'community')
               ->data($invite->toArray(), 'community-invite')
               ->send($user->token);

          return $invite;
     }


     public function accept(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'invite_id' => 'required',
               'community_admin_id' => 'required'
          ]);

          $invite = Invite::with(['community', 'user'])->find($request->input('invite_id'));

          $admin = User::find($request->input('community_admin_id'));


          $invite->delete();

          $invite->community->users()->attach(
               $invite->user,
               ['accepted' => true]
          );

          $user = $invite->user->only(['name', 'avatar', 'id']);

          $user['pivot']['accepted'] = 1;

          $fcm->title($request->user()->name . ' Accepted Your Invite')
               ->body('')
               ->data(['user' => $user, 'comm_id' => $invite->community->id], 'invite-accepted')
               ->send($admin->token);

     }

     public function decline(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'invite_id' => 'required',
               'community_admin_id' => 'required'
          ]);

          $invite = Invite::find($request->input('invite_id'));

          $admin = User::find($request->input('community_admin_id'));


          $invite->delete();

          $fcm->title('Invite Declined')
               ->body($request->user()->name . ' Declined Your Invite')
               ->data($invite->toArray())
               ->send($admin->token);

     }

     public function search(Request $request)
     {
          $community = Community::findOrFail($request->input('community_id'));

          $members = $community->users->pluck('id');

          $users = User::with(['invites' => function ($q) use ($request) {
               return $q->where('community_id', $request->input('community_id'));
          }])->where('name', 'like', '%' . $request->input('n') . '%')->get();


          return $users->filter(function ($user) use ($members) {

               return !$members->contains($user->id);
          })->values();

     }
}