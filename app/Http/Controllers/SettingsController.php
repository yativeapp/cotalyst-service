<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class SettingsController extends Controller
{
     public function user(Request $request)
     {

          $user = $request->user();

          $user['roles'] = ['admin'];

          return ['data' => $user];
     }

     public function username(Request $request)
     {
          $this->validate($request, [
               'username' => 'required'
          ]);

          $request->user()->update([
               'name' => $request->input('username')
          ]);
     }

     public function password(Request $request)
     {
          $this->validate($request, [
               'password' => 'required'
          ]);

          $request->user()->update([
               'passowrd' => Hash::make($request->input('password'))
          ]);
     }

     public function number(Request $request)
     {

          $this->validate($request, [
               'number' => 'required'
          ]);

          $request->user()->update([
               'phone_number' => $request->input('number')
          ]);
     }
}