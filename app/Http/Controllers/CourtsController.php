<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Court;
use Carbon\Carbon;
use App\User;
use App\Extras\FCMProvider;

class CourtsController extends Controller
{

     public function index(Request $request)
     {


          $query = Court::withoutGlobalScopes()->with(['provider', 'activity'])->withCount('schedules');

          if ($request->filled('providerId')) {

               $query->where('user_id', $request->input('providerId'));
          }

          if ($request->filled('approved')) {

               $query->where(
                    'approved',
                    filter_var($request->input('approved'), FILTER_VALIDATE_BOOLEAN)
               );
          }

          $query->orderBy(
               $request->input('sortBy'),
               $request->input('sortOrder')
          );

          return $query->paginate($request->input('limit'));

     }


     public function show($courtId)
     {

          return Court::findOrFail($courtId);
     }

     public function byActivity(Request $request, $providerId, $activityId)
     {
          if ($providerId == 'my') {
               $user = $request->user();
          } else {

               $user = User::find($providerId);
          }


          return $user->courts()->where('activity_id', $activityId)->get();
     }

     public function store(Request $request)
     {

          $this->validate($request, [
               'name' => 'required',
               'price' => 'required',
               'activity_id' => 'required',
          ]);

          return Court::create([
               'name' => $request->input('name'),
               'price' => $request->input('price'),
               'activity_id' => $request->input('activity_id'),
               'user_id' => $request->user()->id,
               'img' => $request->input('imageUrl')
          ]);
     }

     public function immediateStore(Request $request , FCMProvider $fcm)
     {

          $this->validate($request, [
               'name' => 'required',
               'price' => 'required',
               'activity_id' => 'required',
          ]);

          $court  = Court::create([
               'name' => $request->input('name'),
               'price' => $request->input('price'),
               'activity_id' => $request->input('activity_id'),
               'user_id' =>  $request->input('user_id'),
               'img' => $request->input('imageUrl'),
               'approved' => true
          ]);

          $court = Court::withCount('schedules')->with(['provider', 'activity'])->find($court->id);

           $fcm->title('New Court Was Added')
               ->body('Admins Of Cotalysty added New Court In ' . $court->activity->name . ' For You')
               ->data(['court_id' => $court->id , 'activity_id' => $court->activity->id , 'activity_name' => $court->activity->name], 'court-approved')
               ->send([$court->provider->token]);

               return $court;
     }


     public function approve(Request $request, FCMProvider $fcm)
     {

          $court = Court::with('activity')->withoutGlobalScopes()->findOrFail($request->input('courtId'));

          $court->update([
               'approved' => true
          ]);


          $fcm->title('Court Request Approved')
               ->body('Your Request To Add new Court To ' . $court->activity->name . ' Has Been Approved')
               ->data(['court_id' => $court->id , 'activity_id' => $court->activity->id , 'activity_name' => $court->activity->name], 'court-approved')
               ->send($court->provider->token);
     }


     public function decline(Request $request, FCMProvider $fcm)
     {
          $court = Court::withCount('schedules')->withoutGlobalScopes()->findOrFail($request->input('courtId'));

          if($court->schedules_count > 0){

               return response(500);
          }

          $court->delete();


          $fcm->title('Court Request Declined')
               ->body('Your Request To Add new Court To ' . $court->activity->name . ' Has Been Declined')
               ->data(['court_id', $court->id], 'court-removed')
               ->send($court->provider->token);
     }

     public function remove(Request $request, FCMProvider $fcm)
     {
          $court = Court::withoutGlobalScopes()->findOrFail($request->input('courtId'));

          $court->delete();


          $fcm->title('Court removed')
               ->body('Admins removed ' . $court->name . ' Court')
               ->data(['court_id', $court->id], 'court-removed')
               ->send($court->provider->token);
     }

}



// $schedules = $courts->pluck('schedules')->flatten()->groupBy(function ($schedule) {

//      return $schedule->started_at->format('Y-m-d');
// })->map(function ($dayDate) use ($courts) {

//      $reserved = [];

//      $dayDate->each(function ($schedule) use (&$reserved) {

//           $start = $schedule->started_at;


//           while ($start->diffInMinutes($schedule->ended_at) > 0) {

//                $reserved[$schedule->court_id][] = $start->toTimeString();
//                $start->addMinutes(30);
//           }
//      });

//      $courts->each(function ($court) use (&$reserved) {
//           if (!isset($reserved[$court->id])) {
//                $reserved[$court->id] = [];
//           }
//      });


//      return $reserved;
// });