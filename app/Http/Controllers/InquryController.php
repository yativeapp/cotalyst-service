<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use App\Challenge;

use App\Community;
use App\Invite;
use App\Extras\FCMProvider;
use App\Inqury;
use Illuminate\Support\Str;

class InquryController extends Controller
{


     public function index(Request $request)
     {
           return Inqury::with('user')->orderBy('resolved')->paginate($request->input('limit'));
     }

     public function show(Request $request)
     {
           
          return $request->user()->inquries;
     }


     public function store(Request $request , FCMProvider $fcm)
     {
          $this->validate($request , [
               'content' => 'required'
          ]);
           
          $x = $request->user()->inquries()->create([
              'content' => $request->input('content')
          ]);

          return Inqury::find($x->id);
     }

     public function resolve(Request $request , FCMProvider $fcm)
     {
          $this->validate($request , [
               'reply' => 'required'
          ]);
           
         $inqury =  Inqury::with('user')->find( $request->input('id'));

         $reply = $request->input('reply');

         $inqury->update([
               'reply' => $reply,
               'resolved' => true
          ]);

          $msg = Str::limit($reply, 10);

            $fcm->title('Admin Replied To Your Inquery')
                    ->body('Admin: ' . $msg)
                    ->data([], "inqury_resolved")
                    ->send($inqury->user->token);

     }


}