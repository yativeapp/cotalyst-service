<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Governorate;
use Carbon\Carbon;

class ProvidersController extends Controller
{

     public function index(Request $request)
     {

          $query = User::with('activities')->whereType('provider');

          $query->orderBy(
               $request->filled('sortBy') ? $request->input('sortBy') : 'id',
               $request->filled('sortOrder') ? $request->input('sortOrder') : 'desc'
          );


          if ($request->filled('searchKey') && $request->filled('searchBy')) {
               $query->where(
                    $request->input('searchBy'),
                    'like',
                    '%' . $request->input('searchKey') . '%'
               );
          }

          if ($request->filled('activity')) {

               $query->whereHas('activities', function ($q) use ($request) {
                    return $q->where('id', $request->input('activity'));
               });
          }
          if ($request->filled('gov')) {

               $query->whereHas('gov', function ($q) use ($request) {
                    return $q->where('id', $request->input('gov'));
               });
          }

          return $query->paginate($request->input('limit'));
     }

     public function show($id)
     {

          return User::with('gov')->findOrFail($id);
     }

     public function total()
     {
          return User::whereType('provider')->count();

     }

     public function activites($id)
     {
          $user = User::findOrFail($id);

          return $user->activities;
     }

     public function search(Request $request)
     {


          return User::with(['courts', 'reviews'])->whereType('provider')
               ->whereHas('gov', function ($query) use ($request) {
                    return $query->where('governorate_id', $request->input('gov'));
               })->whereHas('activities', function ($query) use ($request) {
                    return $query->where('activity_id', $request->input('activity'));

               })->has('courts')->get();
     }

     public function searchByName(Request $request)
     {

$name = $request->input('name');

          return User::with(['courts', 'reviews'])->whereType('provider')
               ->where('name' , 'like' , "%{$name}%")->get();
     }


     public function store(Request $request)
     {

          $this->validate($request, [
               'name' => 'required',
               'email' => 'required',
               'password' => 'required',
               'gov' => 'required'
          ]);

          $user = User::create([
               'name' => $request->input('name'),
               'email' => $request->input('email'),
               'phone_number' => $request->input('phone_number'),
               'password' => Hash::make($request->input('password')),
               'type' => 'provider'
          ]);

          $user->gov()->attach(Governorate::find($request->input('gov')));

          return $user;
     }

     public function searchUser(Request $request)
     {
          return User::whereType('user')->where('id', '!=', $request->user()->id)->where('name', 'like', '%' . $request->input('n') . '%')->get();
     }

       public function overmonths()
     {
           $sc = User::whereType('provider')->whereYear('created_at' , Carbon::now()->year)->get();

           $months = [];

 
           for($m=1; $m<=12; ++$m){
    $months[date('F', mktime(0, 0, 0, $m, 1))] = 0 ;
}


           $sc = $sc->groupBy(function($sc){
               return $sc->created_at->format('F');
           })->map(function($sc){
                return $sc->count();
           });

           return collect($months)->merge($sc)->values(); 
     }
}