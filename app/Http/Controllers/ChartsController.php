<?php

namespace App\Http\Controllers;

use App\User;




class ChartsController extends Controller
{

     public function gender()
     {

          return [
               'male' => User::whereGender('male')->count(),
               'female' => User::whereGender('female')->count()
          ];
     }

     public function months()
     {

     }

     public function providersUsers()
     {

          return [
               'users' => User::whereType('user')->count(),
               'providers' => User::whereType('provider')->count()
          ];
     }
}