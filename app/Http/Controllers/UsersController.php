<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\User;
use Carbon\Carbon;
class UsersController extends Controller
{

     public function index(Request $request)
     {
 $query = User::with('activities')->whereType('user');

          $query->orderBy(
               $request->filled('sortBy') ? $request->input('sortBy') : 'id',
               $request->filled('sortOrder') ? $request->input('sortOrder') : 'desc'
          );


          if ($request->filled('searchKey') && $request->filled('searchBy')) {
               $query->where(
                    $request->input('searchBy'),
                    'like',
                    '%' . $request->input('searchKey') . '%'
               );
          }

          return $query->paginate($request->input('limit'));
     }


     public function total()
     {

          return User::whereType('user')->count();
     }


     public function show($id)
     {
           
          return User::findOrFail($id);
     }

     public function user(Request $request)
     {

          $user = $request->user();

          $user['roles'] = ['admin'];

          return ['data' => $user];
     }

      public function overmonths()
     {
           $sc = User::whereType('user')->whereYear('created_at' , Carbon::now()->year)->get();

           $months = [];

 
           for($m=1; $m<=12; ++$m){
    $months[date('F', mktime(0, 0, 0, $m, 1))] = 0 ;
}


           $sc = $sc->groupBy(function($sc){
               return $sc->created_at->format('F');
           })->map(function($sc){
                return $sc->count();
           });

           return collect($months)->merge($sc)->values(); 
     }
}