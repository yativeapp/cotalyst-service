<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Challenge;
use App\Score;
use App\Extras\FCMProvider;
use App\Community;

class ScoreController extends Controller
{


     public function store(Request $request, FCMProvider $fcm)
     {

          $winner_id = $request->input('winner_id');

          $other_id = $request->input('other_id');

          $challenge = Challenge::findOrFail($request->input('challenge_id'));

          $comm_id = 0;

          $challenge->update([
               'winner_id' => $winner_id,
               'status' => Challenge::FINISHED
          ]);


          $authId = $request->user()->id;

          if ($challenge->challenger_type == Community::class) {

               $communities = Community::with(['users', 'admin'])->findMany([$winner_id, $other_id]);

               $winner = $communities->where('id', $winner_id)->first();

               $other = $communities->where('id', $other_id)->first();



               $for = 'community';

               $winner->users->each(function ($user) {
                    $user->score()->create([
                         'points' => Score::POINT
                    ]);
               });

               $winner->admin->score()->create([
                    'points' => Score::POINT
               ]);







               $self = $communities->where('id', '!=', $other->id)->first();

               $iswinner = $winner_id == $self->id;

               $msg = $iswinner ? 'You Won ' : 'You Lost ';

               $fcm->title($msg)
                    ->body($msg . $challenge->name . ' Challenge')
                    ->data(['challenge_id' => $challenge->id, 'winner_id' => $winner_id, 'iswinner' => $iswinner, 'for' => $for, 'comm_id' => $comm_id], "score-changed")
                    ->send($self->tokens());


               $tokens = $other->tokens();

               $iswinner = $winner_id == $other->id;


               $comm_id = $other->id;


          } else {
               $for = 'user';

               User::find($winner_id)->score()->create([
                    'points' => Score::POINT
               ]);

               $other = collect([$winner_id, $other_id])->filter(function ($id) use ($request) {
                    return $id != $request->user()->id;
               })->map(function ($id) {
                    return User::find($id);
               })->first();


               $tokens = [$other->token];

               $iswinner = $winner_id == $other->id;



          }


          $msg = $iswinner ? 'You Won ' : 'You Lost ';
          $fcm->title($msg)
               ->body($msg . $challenge->name . ' Challenge')
               ->data(['challenge_id' => $challenge->id, 'winner_id' => $winner_id, 'iswinner' => $iswinner, 'for' => $for, 'comm_id' => $comm_id], "score-changed")
               ->send($tokens);

     }
}