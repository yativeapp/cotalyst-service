<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scheduler;
use App\User;
use Carbon\Carbon;
use App\Court;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Challenge;
use Illuminate\Support\Str;
use App\Extras\FCMProvider;
use App\Community;

class SchedulerController extends Controller
{

     public function index(Request $request)
     {

          $query = Scheduler::with(['court', 'user']);

          if ($request->filled('providerId')) {

               $query->where('user_id', $request->input('providerId'));
          }

          if ($request->filled('trans')) {

               $query->where('receipt_id', $request->input('trans'));
          }

          if ($request->filled('dateRange')) {

               $query->whereDate('started_at', '>', $request->input('dateRange.0'))
                    ->whereDate('started_at', '<', $request->input('dateRange.1'));
          }



          $query->orderBy(
               $request->filled('sortBy') ? $request->input('sortBy') : 'id',
               $request->filled('sortOrder') ? $request->input('sortOrder') : 'desc'
          );

          return $query->paginate($request->input('limit'));
     }

     public function active(Request $request)
     {

          $schedulrers = $request->user()->schedules()
               ->whereDate('started_at', '>=', Carbon::now())
               ->orderBy('started_at', 'asc')
               ->with(['court.activity', 'court.provider.gov'])->get();

          return $schedulrers->groupBy(function ($sc) {
               return $sc->receipt_id;
          });
     }
     public function past(Request $request)
     {

          $schedulrers = $request->user()->schedules()
               ->whereDate('started_at', '<', Carbon::now())
               ->orderBy('started_at', 'asc')
               ->with(['court.activity', 'court.provider.gov'])->get();

          return $schedulrers->groupBy(function ($sc) {
               return $sc->receipt_id;
          });
     }

     public function show(Request $request, $courtId)
     {
          $providerId = $request->input('providerId');
          $started_at = $request->input('started_at');

          $provider = User::find($providerId);

          $court = $provider->courts()
               ->with(['schedules' => function ($query) use ($started_at) {

                    return $query->with('user')->whereDate(
                         'started_at',
                         Carbon::parse($started_at)
                    );
               }])->where([
                    'id' => $courtId
               ])->first();


          return $court->schedules->map(function ($schedule) {

               return $schedule->started_at->toTimeString();
          });
     }

     public function showForProvider(Request $request, $courtId)
     {
          $started_at = $request->filled('started_at') ? $request->input('started_at') : Carbon::now();

          $provider = $request->user();

          $court = Court::with(['schedules' => function ($query) use ($started_at) {


               return $query->with('user')->whereDate(
                    'started_at',
                    $started_at
               );
          }])->where([
               'id' => $courtId
          ])->first();


          $reserved = [];


          $sc = $court->schedules ?? collect([]);

          return $sc->map(function ($schedule) {

               return [
                    'id' => $schedule->id,
                    'time' => $schedule->started_at->toTimeString(),
                    'user' => $schedule->user,
                    'receipt_id' => $schedule->receipt_id
               ];
          });
     }

     public function store(Request $request, FCMProvider $fcm)
     {
          $this->validate($request, [
               'court_id' => 'required',
               'date' => 'required',
               'selected_hours' => 'required'
          ]);

          if ($request->filled('challenge_id')) {
               $challenge = Challenge::with('challenger')->find($request->input('challenge_id'));


               $challenge->schedules()->delete();

               $challenge->update([
                    'provider_id' => $request->input('provider_id')
               ]);
          }

          $court = Court::with(['provider', 'activity'])->find($request->input('court_id'));

          $selectedHours = collect($request->input('selected_hours'));




          $receipt_id = Str::random();

          $sch = null;

          $selectedHours->each(function ($hour) use (&$sch, $request, $court, $receipt_id) {

               $started_at = Carbon::parse($request->input('date'));

               $started_at->setTime(...explode(':', $hour));

               $ended_at = Carbon::parse($started_at);

               $ended_at->addHour();

               $sch = $request->user()->schedules()->create([
                    'court_id' => $court->id,
                    'started_at' => $started_at->toDateTimeString(),
                    'ended_at' => $ended_at->toDateTimeString(),
                    'court_id' => $request->input('court_id'),
                    'receipt_id' => $receipt_id,
                    'challenge_id' => $request->input('challenge_id')
               ]);
          });

          if ($court->provider->token) {
               $data = [
                    'date' => Carbon::parse($request->input('date'))->toDateString(),
                    'court' => $request->input('court_id'),
                    'activity_id' => $court->activity->id,
                    'activity_name' => $court->activity->name,
                    'hours' => $selectedHours->toArray(),
                    'user' => $request->user()->only(['name', 'avatar']),
                    'count' => $selectedHours->count()
               ];
               $fcm->title('New Reservation')
                    ->body($request->user()->name . " Has New Resevation in " . $court->name)
                    ->data($data, 'court-new-reservations')
                    ->send($court->provider->token);
          }


          if ($request->filled('challenge_id')) {

               $this->handleChallengeSchedulers($request, $fcm, $challenge);
          }


          return $sch;
     }


     public function handleChallengeSchedulers($request, $fcm, $challenge)
     {



          $match = $challenge->matches->where('selected', true)->first();

          if ($match->matchable_type == Community::class) {

               $for = 'community';

               $fcm->title('Schedulers Set For Your Challenge')
                    ->body($request->user()->name . " Has set The Schedulers For" . $challenge->name . " Challenge")
                    ->data(['challenge_id' => $challenge->id], 'challenge-schedulers-set')
                    ->send($challenge->challenger->tokens());


               $tokens = $match->matchable->users->pluck('token')->toArray();
          } else if ($match->matchable_type == User::class) {

               $for = 'user';



               $other = collect([$match->matchable->id, $challenge->challenger_id])->filter(function ($id) use ($request) {
                    return $id != $request->user()->id;
               })->map(function ($id) {
                    return User::find($id);
               })->first();

               $tokens = [$other->token];
          }

          $fcm->title('Schedulers Set For Your Challenge')
               ->body($request->user()->name . " Has set The Schedulers For" . $challenge->name . " Challenge")
               ->data(['challenge_id' => $challenge->id], 'challenge-schedulers-set')
               ->send($tokens);
     }

     public function manual(Request $request)
     {
          $this->validate($request, [
               'username' => 'required',
               'court_id' => 'required',
               'started_at' => 'required'
          ]);

          $user = User::create(['name' => $request->input('username'), 'type' => 'anonymous']);

          $court_id = $request->input('court_id');

          $selectedHours = collect($request->input('selected_hours'));

          $receipt_id = Str::random();


          $selectedHours->each(function ($hour) use ($user, $request, $court_id, $receipt_id) {

               $started_at = Carbon::parse($request->input('started_at'));

               $started_at->setTime(...explode(':', $hour));

               $ended_at = Carbon::parse($started_at);

               $ended_at->addHour();

               return $user->schedules()->create([
                    'court_id' => $court_id,
                    'started_at' => $started_at->toDateTimeString(),
                    'ended_at' => $ended_at->toDateTimeString(),
                    'receipt_id' => $receipt_id,
               ]);
          });

          return compact('receipt_id');
     }

     public function destroy(Request $request, FCMProvider $fcm)
     {

          $sc = Scheduler::with(['court.provider', 'court.activity'])->where('receipt_id', $request->input('receipt_id'));


          $schedule =  $sc->get();

          $sc->delete();


          if ($schedule->first()->court->provider->token) {
               $data = [
                    'date' => Carbon::parse($request->input('date'))->toDateString(),
                    'court' => $schedule->first()->court->id,
                    'receipt_id' => $request->input('receipt_id'),
                    'count' => $schedule->count(),
                    'activity_id' => $schedule->first()->court->activity->id,
                    'activity_name' => $schedule->first()->court->activity->name
               ];
               $fcm->title('Reservation Canceled')
                    ->body($request->user()->name . " Cacnceled His Reservation in " . $schedule->first()->court->name)
                    ->data($data, 'court-remove-reservations')
                    ->send([$schedule->first()->court->provider->token]);
          }
     }

     public function destroyUserReservation(Request $request, FCMProvider $fcm)
     {

          $sc = Scheduler::with(['user', 'court'])->where('receipt_id', $request->input('receipt_id'));


          $schedule =  $sc->get();

          $sc->delete();

          if ($schedule->first()->user->type != 'anonymous') {
               $data = [
                    'receipt_id' => $request->input('receipt_id'),
               ];
               $fcm->title('Reservation Canceled')
                    ->body($request->user()->name . " Cacnceled Your Reservation in " . $schedule->first()->court->name)
                    ->data($data, 'court-remove-reservations')
                    ->send([$schedule->first()->user->token]);
          }
     }

     public function total()
     {

          return Scheduler::count();
     }



     public function overmonths()
     {
          $sc = Scheduler::whereYear('started_at', Carbon::now()->year)->get();

          $months = [];


          for ($m = 1; $m <= 12; ++$m) {
               $months[date('F', mktime(0, 0, 0, $m, 1))] = 0;
          }


          $sc = $sc->groupBy(function ($sc) {
               return $sc->started_at->format('F');
          })->map(function ($sc) {
               return $sc->count();
          });

          return collect($months)->merge($sc)->values();
     }
}
