<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Governorate;
use App\Review;
use App\Extras\FCMProvider;

class ReviewController extends Controller
{

     public function index(Request $request)
     {

          $reviews = $request->user()->reviews()->with('user')->get();


          return [
               'reviews' => $reviews,
               'avg' => round($reviews->avg('rate') , 1)?? 0
          ];
     }

     public function store(Request $request , FCMProvider $fcm)
     {
          $this->validate($request, [
               'provider_id' => 'required',
               'rate' => 'required'
          ]);

          $provider = User::find($request->input('provider_id'));

          $rate = $request->input('rate');

          $feedback = $request->input('feedback');

          $user = $request->user()->only(['name' , 'avatar']);

          $avg = round($provider->reviews->avg('rate') , 1);

           $fcm->title('New Review')
                    ->body($request->user()->name . ' Reviewed You With ' . $rate . ' Points')
                    ->data(['user' => $user ,'avg' => $avg , 'rate' => $rate, 'feedback' => $feedback], "provider-new-review")
                    ->send($provider->token);

          return $provider->reviews()->create([
               'user_id' => $request->user()->id,
               'feedback' => $feedback,
               'rate' => $rate
          ]);
     }
}