<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Socialite;

class AuthController extends Controller
{



     public function login(Request $request)
     {

          $this->validate($request, [
               'email' => 'required',
               'password' => 'required'
          ]);

          $user = User::whereType('user')->whereEmail($request->input('email'))->first();


          if (!$user) return response('Wrong Creds', 500);

          if ((Hash::check($request->input('password'), $user->password))) {

               $token = $user->createToken('cotalysty')->accessToken;

               return response()->json([
                    'user' => $user,
                    'access_token' => $token
               ]);
          } else {
               return response('Wrong Creds', 500);
          }
     }

     public function fbLogin(Request $request)
     {
          $this->validate($request, [
               'token' => 'required'
          ]);

          $fbUser = Socialite::driver('facebook')->stateless()->userFromToken($request->input('token'));


          $user = User::firstOrNew(['email' => $fbUser->getEmail()]);

          if ($user->exists) {

               $token = $user->createToken('cotalysty')->accessToken;
          } else {

               $user->fill([
                    'name' => $fbUser->getName(),
                    'email' => $fbUser->getEmail(),
                    'avatar' => $fbUser->getAvatar()
               ]);

               $user->save();

               $token = $user->createToken('cotalysty')->accessToken;
          }

          return response()->json([
               'user' => $user,

               'access_token' => $token
          ]);
     }

     public function googleLogin(Request $request)
     {
          $this->validate($request, [
               'email' => 'required',
               'username' => 'required'
          ]);


          $user = User::firstOrNew(['email' => $request->input('email')]);

          if ($user->exists) {

               $token = $user->createToken('cotalysty')->accessToken;
          } else {

               $user->fill([
                    'name' => $request->input('username'),
                    'email' => $request->input('email'),
               ]);

               $user->save();

               $token = $user->createToken('cotalysty')->accessToken;
          }

          return response()->json([
               'user' => $user,

               'access_token' => $token
          ]);
     }

     public function providerLogin(Request $request)
     {

          $this->validate($request, [
               'email' => 'required',
               'password' => 'required'
          ]);

          $user = User::whereType('provider')->whereEmail($request->input('email'))->first();



          if (!$user) return response('Wrong Creds', 500);

          if ((Hash::check($request->input('password'), $user->password))) {

               $token = $user->createToken('cotalysty')->accessToken;

               return response()->json([
                    'user' => $user,

                    'access_token' => $token
               ]);
          } else {
               return response('Wrong Creds', 500);
          }
     }

     public function signup(Request $request)
     {
          $this->validate($request, [
               'email' => 'required',
               'password' => 'required',
               'name' => 'required'
          ]);

           $user = User::create([
               'email' => $request->input('email'),
               'name' => $request->input('name'),
               'password' => Hash::make($request->input('password')),
               'type' => 'user'
          ]);

          $user = User::find($user->id);
          
          return [
               'user' => $user,

               'access_token' => $user->createToken('cotalysty')->accessToken
          ];
     }


     public function updateToken(Request $request)
     {

          $this->validate($request, [
               'token' => 'required'
          ]);

          $request->user()->update([
               'token' => $request->input('token')
          ]);
     }
     public function deleteToken(Request $request)
     {

          User::where(['type' => 'user' , 'id' => $request->input('id')])->update([
               'token' => null
          ]);
     }

     public function updateAvatar(Request $request)
     {

          $this->validate($request, [
               'imageUrl' => 'required'
          ]);

          $request->user()->update([
               'avatar' => $request->input('imageUrl')
          ]);
     }



}