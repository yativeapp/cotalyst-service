<?php

namespace App\Http\Controllers;

use App\User;
use App\Community;
use Illuminate\Http\Request;
use App\Extras\FCMProvider;
use App\Challenge;

class CommunityController extends Controller
{

     public function index(Request $request)
     {
          $userId = !$request->filled('userId') ?? $request->input('userId');


          if ($request->filled('userId')) {

               $query = Community::where('user_id', $userId)
                    ->orWhereHas('users', function ($query) use ($userId) {
                         return $query->where([
                              'user_id' => $userId,
                              'accepted' => true
                         ]);
                    });

          }

          $query = Community::orderBy(
               $request->filled('sortBy') ? $request->input('sortBy') : 'id',
               $request->filled('sortOrder') ? $request->input('sortOrder') : 'desc'
          );


          if ($request->filled('searchKey') && $request->filled('searchBy')) {

               $query->where(
                    $request->input('searchBy'),
                    'like',
                    '%' . $request->input('searchKey') . '%'
               );
          }



          return $query->paginate($request->input('limit'));

     }
     public function user(Request $request)
     {
          $userId = $request->filled('userId') ? $request->input('userId') : $request->user()->id;


          return Community::where('user_id', $userId)
               ->orWhereHas('users', function ($query) use ($userId) {
                    return $query->where([
                         'user_id' => $userId,
                         'accepted' => true
                    ]);
               })->get();


     }

     public function store(Request $request)
     {
          $this->validate($request, [
               'name' => 'required',
          ]);

          return $request->user()->communities()->create([
               'name' => $request->input('name'),
          ]);
     }

     public function show($id)
     {

          return Community::findOrFail($id);
     }


     public function getUsers($id)
     {

          $community = Community::with('users')->findOrFail($id);

          return $community->users;
     }

     public function challenges($id)
     {


          return Challenge::where([
               'challenger_type' => Community::class,
               'challenger_id' => $id
          ])->orWhereHas('matches', function ($q) use ($id) {

               return $q->where([
                    'matchable_type' => Community::class,
                    'matchable_id' => $id,
                    'selected' => true
               ]);
          })->get();

     }

     public function search(Request $request)
     {
          $this->validate($request, [
               'q' => 'present',
          ]);

          $q = $request->input('q');

          $userId = $request->user()->id;


          return Community::with(['users'])
               ->where('user_id', '!=', $userId)
               ->orWhereHas('users', function ($query) use ($userId) {
                    return $query->where('user_id', '!=', $userId);
               })
               ->where('name', 'like', "%$q%")->get();

     }

     public function removeUser(Request $request, FCMProvider $fcm)
     {

          $community = Community::find($request->input('communityId'));
          $user = User::findOrFail($request->input('userId'));

          $member = $community->users()->where('user_id', $request->input('userId'))->first();



          if ($member->pivot->accepted == 1) {

               $title = 'You Have Been Kiked Off';
               $body = $community->name . " Admin Has Kiked You";
          } else {
               $title = 'Request Declined';
               $body = $community->name . " Admin Has Declined Your Request";
          }

          $community->users()->detach(
               $user
          );

          $data = [
               'id' => $community->id,
          ];

          $fcm->title($title)
               ->body($body)
               ->data($data, 'comm-kik')
               ->send($user->token);

     }

     public function joinUser(Request $request, FCMProvider $fcm)
     {
          $community = Community::find($request->input('communityId'));
          $user = $request->user();

          $community->users()->attach(
               $user,
               ['accepted' => false]
          );

          $data = [
               'id' => $user->id,
               'name' => $user->name,
               'avatar' => $user->avatar,
               'pivot' => [
                    'accepted' => 0
               ],
               'community' => [
                    'id' => $community->id
               ]
          ];


          $fcm->title('Join Request')
               ->body($user->name . " Sent Join Request To " . $community->name)
               ->data($data, 'join-comm-request')
               ->send($community->admin->token);
     }

     public function acceptUser(Request $request, FCMProvider $fcm)
     {
          $community = Community::withCount('users')->find($request->input('communityId'));

          $user = User::findOrFail($request->input('userId'));

          $community->users()->updateExistingPivot(
               $user,
               ['accepted' => true]
          );

          $data = [
               'id' => $community->id,
          ];


          $fcm->title('Request Accepted')
               ->body("You're Now Part Of  " . $community->name . " Community")
               ->data($data, 'join-comm-accepted')
               ->send($user->token);
     }



}