<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;
use App\User;


class ActivityController extends Controller
{


     public function index(Request $request)
     {
          $query = Activity::withCount(['providers', 'challenges']);

          if ($request->has('with_providers')) {
               $query->with(['providers' => function ($query) {
                    return $query->select(['id', 'user_id'])->has('courts');
               }]);
          }

          if ($request->has('with_providers')) {
               $result = $query->get();

               return $result->filter(function ($activity) {

                    return $activity->providers->count();
               })->values();
          } else {

               return $query->get();
          }
     }


     public function show($providerId = null)
     {

          $provider = User::findOrFail($providerId);

          return $provider->activities;
     }

     public function showForProvider(Request $request)
     {

          return $request->user()->activities()->withCount(['courts' => function ($query) use ($request) {

               return $query->where('user_id', $request->user()->id);
          }, 'schedules'])->get();
     }



     public function store(Request $request)
     {

          $this->validate($request, [
               'name' => 'required',
               'icon' => 'required'
          ]);

          return Activity::create([
               'name' => $request->input('name'),
               'icon' => $request->input('icon')
          ]);
     }


     public function destroy(Request $request)
     {

          Activity::destroy($request->input('id'));
     }


     public function assign(Request $request)
     {

          $provider = User::findOrFail($request->input('providerId'));

          $provider->activities()->sync($request->input('activities'));
     }
}
