<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use App\Challenge;

use App\Community;
use App\Extras\FCMProvider;
use App\Match;


class MatchController extends Controller
{

     public function show($id)
     {

          return Match::with('matched')->findOrFail($id);
     }

     public function store(Request $request, $challengeid, FCMProvider $fcm)
     {
          $challenge = Challenge::with('challenger')->find($challengeid);

          if ($challenge->status == Challenge::STARTED) {

               return response('Sorry This Challenge Has Been Started', 500);
          }

          $entity = $request->has('community_id') ? Community::find($request->input('community_id')) : $request->user();

          $match = $entity->matches()->create([
               'challenge_id' => $challengeid,
               'selected' => false
          ]);


          $match->load('matched');

          $comm_id = 0;

          if ($request->has('community_id')) {

                //self members notify
               $for = 'community';

               $fcm->title('Challenge Request Sent')
                    ->body($request->user()->name . ' Sent Challenge Request in ' . $challenge->challenger->name . ' To ' . $match->matchable->name)
                    ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $challenge->challenger->id], 'new-match')
                    ->send($challenge->challenger->tokens());

                    ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();


          } else {

               $for = 'user';

               $tokens = [$challenge->challenger->token];
          }



          $fcm->title('New Match for Your Challenge')
               ->body($request->user()->name . ' Sent You A Match ')
               ->data([
                    'challenge_id' => $challenge->id,
                    'match_id' => $match->id,
                    'for' => $for
               ], 'new-match')
               ->send($tokens);


          // return $challenge->load(['matches', 'activity', 'gov', 'challenger']);
     }


     public function destroy(Request $request, FCMProvider $fcm)
     {
          $challengeid = $request->input('challengeId');

          $challenge = Challenge::with('challenger')->find($challengeid);


          $entity = $request->filled('community_id') ? Community::find($request->input('community_id')) : $request->user();

          $match = $entity->matches()->where([
               'challenge_id' => $challengeid,
          ])->first();



          $match->delete();



          $comm_id = 0;

          if ($request->filled('community_id')) {

          //self members notify
               $for = 'community';

               $fcm->title('Challenge Request Canceled')
                    ->body(Auth::user()->name . ' Cancel Challenge Request in ' . $challenge->challenger->name . ' From ' . $match->matchable->name)
                    ->data(['challenge_id' => $challenge->id, 'match_id' => $match->id, 'for' => $for, 'comm_id' => $challenge->challenger->id], 'match-canceled')
                    ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();

          } else {

               $for = 'user';

               $tokens = [$challenge->challenger->token];
          }

          $fcm->title('Challenge Request Canceled')
               ->body($challenge->challenger->name . ' Canceled His Challenge Request')
               ->data(['challenge_id' => $challenge->id, 'match_id' => $match->id, 'for' => $for, 'comm_id' => $match->matchable->id], 'match-canceled')
               ->send($tokens);
     }
}