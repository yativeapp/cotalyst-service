<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Governorate;

class GovernorateController extends Controller
{

     public function index(Request $request)
     {
          $query = Governorate::withCount(['providers', 'users', 'challenges']);

          if ($request->has('with_providers')) {
               $query->with(['providers' => function ($query) {
                    return $query->select(['id', 'placable_id', 'placable_type'])->has('courts');
               }]);
          }

          if ($request->has('with_providers')) {
               $result = $query->get();


               return $result->filter(function ($gov) {

                    return $gov->providers->count();
               })->values();
          } else {

               return $query->get();
          }
     }

     public function store(Request $request)
     {

          $this->validate($request, [
               'name' => 'required'
          ]);

          return Governorate::create(['name' => $request->input('name')]);
     }

     public function destroy(Request $request)
     {

          Governorate::destroy($request->input('id'));
     }
}
