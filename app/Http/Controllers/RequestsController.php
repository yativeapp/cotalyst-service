<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Governorate;
use App\Challenge;
use App\Community;
use App\Extras\FCMProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RequestsController extends Controller
{

     public function store(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'activity_id' => 'required',
               'gov_id' => 'required'
          ]);

          $entity = $request->input('community_id') != 0 ? Community::find($request->input('community_id')) : $request->user();

          if ($request->has('challenge_at')) {
               $timestamp = Carbon::parse($request->input('challenge_at'));

          } else {

               $timestamp = Carbon::now();
          }


          $challengable = ($request->input('challengable') == 'user') ? 'App\\User' : 'App\\Community';


          $challengable = $challengable::find($request->input('challengable_id'));


          $challenge = $entity->challenges()->create([
               'challenge_at' => $timestamp->toDateTimeString(),
               'activity_id' => $request->input('activity_id'),
               'gov_id' => $request->input('gov_id'),
               'status' => Challenge::REQUESTED,
          ]);


          $challengable->matches()->create([
               'challenge_id' => $challenge->id,
               'selected' => true
          ]);

               //notify other
          $match = $challenge->matches()->with('matched')->first();

          $comm_id = 0;

          if ($match->matchable_type == Community::class) {

               //self members notify
               $for = 'community';

               $fcm->title('New Challenge Request')
                    ->body(Auth::user()->name . ' Sent A Challenge Request in ' . $entity->name . ' To ' . $challengable->name)
                    ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $entity->id], 'new-challenge-request')
                    ->send($entity->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $challengable->tokens();

          } else if ($match->matchable_type == User::class) {

               $for = 'user';

               $tokens = [$match->matchable->token];
          }


          $fcm->title('New Challenge Request')
               ->body($entity->name . ' Sent You A Challenge Request ')
               ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $entity->id], 'new-challenge-request')
               ->send($tokens);

          return $challenge->load(['matches', 'activity', 'gov', 'challenger']);
     }

     public function accept(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'challengeId' => 'required'
          ]);


          $challenge = Challenge::with('challenger')->find($request->input('challengeId'));

          $challenge->update([
               'status' => Challenge::STARTED
          ]);

          //notify other
          $match = $challenge->matches()->with('matched')->where('id', $request->input('matchId'))->first();

     
          $match->update([
               'selected' => true
          ]);

          $comm_id = 0;

          if ($match->matchable_type == Community::class) {

              //self members notify
               $for = 'community';

               $fcm->title('New Challenge Started')
                    ->body(Auth::user()->name . ' Accept Challenge Request in ' . $challenge->challenger->name . ' From ' . $match->matchable->name)
                    ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $challenge->challenger->id], 'challenge-request-accepted')
                    ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();

          } else if ($match->matchable_type == User::class) {

               $for = 'user';


               $tokens = [$challenge->challenger->token];
          }


          $fcm->title('Challenge Request Accepted')
               ->body('Your Challenge With ' . $match->matchable->name . ' Has Been Started')
               ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $comm_id], 'challenge-request-accepted')
               ->send($tokens);


     }

     public function cancel(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'challengeId' => 'required'
          ]);

          $challenge = Challenge::with(['challenger', 'matches.matched'])->find($request->input('challengeId'));

          $match = $challenge->matches->first();


          $challenge->delete();

          $comm_id = 0;

          if ($match->matchable_type == Community::class) {

          //self members notify
               $for = 'community';

               $fcm->title('Challenge Request Canceled')
                    ->body(Auth::user()->name . ' Cancel Challenge Request in ' . $challenge->challenger->name . ' From ' . $match->matchable->name)
                    ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $challenge->challenger->id], 'challenge-canceled')
                    ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $match->matchable->tokens();

          } else if ($match->matchable_type == User::class) {

               $for = 'user';

               $tokens = [$match->matchable->token];
          }

          $fcm->title('Challenge Request Canceled')
               ->body($challenge->challenger->name . ' Canceled His Challenge Request')
               ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $match->matchable->id], 'challenge-canceled')
               ->send($tokens);
     }

     public function decline(Request $request, FCMProvider $fcm)
     {

          $this->validate($request, [
               'challengeId' => 'required'
          ]);

          $challenge = Challenge::with(['challenger', 'matches.matched'])->find($request->input('challengeId'));

          $match = $challenge->matches->first();


          $challenge->delete();

          $comm_id = 0;

          if ($match->matchable_type == Community::class) {

          //self members notify
               $for = 'community';

               $fcm->title('Challenge Declined')
                    ->body(Auth::user()->name . ' Declined Challenge Request in ' . $challenge->challenger->name . ' From ' . $match->matchable->name)
                    ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $challenge->challenger->id], 'new-challenge-request')
                    ->send($challenge->challenger->tokens());

               ///

               $comm_id = $match->matchable->id;

               $tokens = $challengable->tokens();

          } else if ($match->matchable_type == User::class) {

               $for = 'user';

               $tokens = [$challenge->challenger->token];
          }

          $fcm->title('Challenge Request Declined')
               ->body($challenge->challenger->name . ' Declined Your Challenge Request')
               ->data(['challenge_id' => $challenge->id, 'for' => $for, 'comm_id' => $match->matchable->id], 'challange-declined')
               ->send($tokens);
     }
}