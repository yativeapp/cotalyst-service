<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use App\Challenge;
use App\Community;
use App\Extras\FCMProvider;
use Auth;

class ChatController extends Controller
{


     public function notify(Request $request, FCMProvider $fcm)
     {
          $this->validate($request, [
               'challengeId' => 'required',
               'message' => 'required'
          ]);

          $challenge = Challenge::with(['challenger' , 'matches.matched'])->find($request->input('challengeId'));

          $match = $challenge->matches->first();

          $msg = Str::limit($request->input('message'), 10);


          if ($match->matchable_type == Community::class) {

                //self members notify

               $fcm->title(Auth::user()->name . ' : ' . $challenge->name)
                    ->body($msg)
               ->data(['challenge_id' => $challenge->id], 'chat')
                    ->send($challenge->challenger->tokens());

               ///


               $tokens = $match->matchable->tokens();

          } else if ($match->matchable_type == User::class) {

             
               $tokens = collect([$match->matchable , $challenge->challenger])->filter(function($user){
return $user->id != Auth::user()->id;
               })->pluck('token')->toArray();


          }


 $fcm->title(Auth::user()->name . ' : ' . $challenge->name)
                    ->body($msg)
               ->data(['challenge_id' => $challenge->id], 'chat')
                    ->send($tokens);

     }
}