<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'token', 'avatar', 'phone_number'
    ];

    protected $with = ['score'];

    protected $appends = ['pricerange' , 'rate'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'score'
    ];

    public function getPriceRangeAttribute($query)
    {


        if ($this->type == 'provider') {
            $courts = $this->courts;
            return $courts->min('price') . ' EGP - ' . $courts->max('price') . ' EGP';
        }
    }

    public function getAvatarAttribute($attr)
    {


        if (is_null($attr)) {
            return "https://ui-avatars.com/api/?name={$this->name}";
        }

        return $attr;
    }

    public function getRateAttribute($attr)
    {


        return $this->score->avg('points') ?? 0;
    }


    public function activities()
    {

        return $this->belongsToMany(Activity::class);
    }

    public function communities()
    {

        return $this->hasMany(Community::class, 'user_id');
    }

    public function challenges()
    {
        return $this->morphMany(Challenge::class, 'challenger');
    }

    public function matches()
    {

        return $this->morphMany(Match::class, 'matchable');
    }
    public function schedules()
    {

        return $this->hasMany(Scheduler::class);
    }

    public function courts()
    {

        return $this->hasMany(Court::class, 'user_id');
    }

    public function gov()
    {

        return $this->morphToMany(Governorate::class, 'placable')->limit(1);
    }

    public function reviews()
    {

        return $this->hasMany(Review::class, 'provider_id');
    }

    public function invites()
    {

        return $this->hasMany(Invite::class, 'user_id');
    }
    public function inquries()
    {

        return $this->hasMany(Inqury::class, 'user_id');
    }

    public function roles()
    {

        return $this->belongsTo(Role::class, 'user_id');
    }


    public function score()
    {

         
        return $this->morphMany(Score::class , 'scorable');
    }


}
