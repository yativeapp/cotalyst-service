<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Activity extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'name', 'icon'
     ];


     public function courts()
     {

          return $this->hasMany(Court::class);
     }

     public function providers()
     {

          return $this->belongsToMany(User::class);
     }
     public function challenges()
     {

          return $this->hasMany(Challenge::class);
     }

     function schedules()
    {
        return $this->hasManyThrough('App\Scheduler', 'App\Court');
    }
}
