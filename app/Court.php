<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Court extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'user_id', 'activity_id', 'name', 'price', 'img', 'approved'
     ];

     protected $casts = [
          'approved' => 'boolean'
     ];

     protected static function boot()
     {
          parent::boot();

          static::addGlobalScope('approved', function (Builder $builder) {
               $builder->where('approved', 1);
          });
     }

     public function provider()
     {

          return $this->belongsTo(User::class, 'user_id');
     }

     public function schedules()
     {

          return $this->hasMany(Scheduler::class);
     }

     public function activity()
     {

          return $this->belongsTo(Activity::class);
     }
}
