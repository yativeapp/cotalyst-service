<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Governorate extends Model
{

     public $timestamps = false;


     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'user_id', 'activity_id', 'name'
     ];


     public function users()
     {

          return $this->morphedByMany(User::class, 'placable')
               ->where('type', 'user');
     }
     public function providers()
     {

          return $this->morphedByMany(User::class, 'placable')
               ->where('type', 'provider');
     }
     public function challenges()
     {

          return $this->morphedByMany(Challenge::class, 'placable');
     }
}