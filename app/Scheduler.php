<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Scheduler extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'user_id', 'court_id', 'started_at', 'ended_at', 'receipt_id', 'challenge_id'
     ];

     public $timestamps = false;

     public $dates = ['started_at', 'ended_at'];

     protected $appends = ['dateformatted', 'start_at_hour', 'end_at_hour'];


     public function getDateFormattedAttribute()
     {

          return Carbon::parse($this->started_at)->toDateString();
     }

     public function getStartAtHourAttribute()
     {

          return $this->started_at->format('g:i A');
     }

     public function getEndAtHourAttribute()
     {

          return $this->ended_at->format('g:i A');
     }

     public function user()
     {

          return $this->belongsTo(User::class);
     }

     public function court()
     {

          return $this->belongsTo(Court::class);
     }

    
  
}
