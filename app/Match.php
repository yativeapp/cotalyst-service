<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Match extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'challenge_id', 'selected'
     ];

     protected $with = ['matched'];

     protected $casts = ['selected' => 'boolean'];

     public function matched()
     {

          return $this->morphTo('matchable');
     }
}