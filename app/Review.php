<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Review extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'user_id', 'feedback', 'rate', 'provider_id'
     ];


     public static function boot()
     {

          parent::boot();

          static::created(function ($review) {

               $review->user()->update([
                    'rate' => static::avg('rate')
               ]);
          });
     }

     public function user()
     {

          return $this->belongsTo(User::class);
     }
}