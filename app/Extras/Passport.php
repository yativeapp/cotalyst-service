<?php 

namespace App\Extras;

use \Dusterio\LumenPassport\LumenPassport;
use \Dusterio\LumenPassport\RouteRegistrar;
use Laravel\Lumen\Application;

class Passport extends LumenPassport
{

     /**
      * All Of This Just To Remove A Bug
      * 
      * Get a Passport route registrar.
      *
      * @param  callable|Router|Application  $callback
      * @param  array  $options
      * @return RouteRegistrar
      */
     public static function routes($callback = null, array $options = [])
     {

          // this is the bug it check for laravel version 5.6 but current version is 5.7
          // if ($callback instanceof Application && preg_match('/5\.[56]\..*/', $callback->version())) $callback = $callback->router;

          if ($callback instanceof Application && preg_match('/5\.[57]\..*/', $callback->version())) $callback = $callback->router;


          $callback = $callback ? : function ($router) {
               $router->all();
          };

          $defaultOptions = [
               'prefix' => 'oauth',
               'namespace' => '\Laravel\Passport\Http\Controllers',
          ];

          $options = array_merge($defaultOptions, $options);


          $callback->group(array_except($options, ['namespace']), function ($router) use ($callback, $options) {
               $routes = new RouteRegistrar($router, $options);
               $routes->all();
          });
     }
}