<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Invite extends Model
{
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
          'community_id', 'user_id'
     ];

     public $timestamps = false;


     public function community()
     {

          return $this->belongsTo(Community::class);
     }
     public function user()
     {

          return $this->belongsTo(User::class);
     }
}