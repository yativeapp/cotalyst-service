<?php 

header("Access-Control-Allow-Origin: *");


header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE, PATCH');
header("Access-Control-Allow-Credentials: true");

header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header("Access-Control-Max-Age: 86400");