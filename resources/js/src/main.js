import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon
import '@/permission' // permission control

import * as firebase from 'firebase/app';

Vue.use(ElementUI, {
  locale
})

Vue.config.productionTip = false



Vue.prototype.$firebase = firebase.initializeApp({
  apiKey: "AIzaSyCWp7tLDKLBfoYYaUA_QyKMfiQPidQstXw",
  authDomain: "catalysty-269e2.firebaseapp.com",
  databaseURL: "https://catalysty-269e2.firebaseio.com",
  projectId: "catalysty-269e2",
  storageBucket: "catalysty-269e2.appspot.com",
  messagingSenderId: "76481079587"
});


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
