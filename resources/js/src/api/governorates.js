import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/governorates',
    method: 'get',
    params
  })
}
export function create(data) {
  return request({
    url: '/governorates',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/governorates',
    method: 'delete',
    data
  })
}
