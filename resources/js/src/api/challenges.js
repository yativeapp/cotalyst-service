import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/challenges',
    method: 'get',
    params
  })
}

export function total() {
  return request({
    url: '/challenges/total',
    method: 'get'
  })
}

export function matches(id) {
  return request({
    url: `/challenge/${id}/matches`,
    method: 'get'
  })
}
