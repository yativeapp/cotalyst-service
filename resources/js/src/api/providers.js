import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/providers',
    method: 'get',
    params
  })
}

export function get(id) {
  return request({
    url: '/providers/' + id,
    method: 'get'
  })
}

export function activities(id) {
  return request({
    url: '/providers/' + id + '/activities',
    method: 'get'
  });
}

export function total() {
  return request({
    url: '/providers/total',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/providers',
    method: 'post',
    data
  })
}

export function overmonths() {
  return request({
    url: '/providers/overmonths',
    method: 'get'
  })
}

export function searchByName(params) {
  return request({
    url: '/providers/search/name',
    method: 'get',
    params
  })
}
