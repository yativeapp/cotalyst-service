import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/users',
    method: 'get',
    params
  })
}

export function get(id) {
  return request({
    url: '/user/get/' + id,
    method: 'get'
  })
}

export function total() {
  return request({
    url: '/users/total',
    method: 'get'
  })
}

export function overmonths() {
  return request({
    url: '/users/overmonths',
    method: 'get'
  })
}
