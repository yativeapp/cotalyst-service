import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: 'inquries/all',
    method: 'get',
    params
  })
}

export function resolve(data) {
  return request({
    url: 'inquries/resolve',
    method: 'post',
    data
  })
}
