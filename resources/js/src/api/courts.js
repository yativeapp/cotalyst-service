import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/courts',
    method: 'get',
    params
  })
}

export function approve(id) {
  return request({
    url: '/courts/approve',
    method: 'patch',
    data: {
      courtId: id
    }
  })
}

export function decline(id) {
  return request({
    url: '/courts/decline',
    method: 'patch',
    data: {
      courtId: id
    }
  })
}
export function remove(id) {
  return request({
    url: '/courts/remove',
    method: 'patch',
    data: {
      courtId: id
    }
  })
}

export function create(data) {
  return request({
    url: '/courts/admin/create',
    method: 'post',
    data
  })
}
