import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/communities',
    method: 'get',
    params
  })
}

export function members(id) {
  return request({
    url: `/community/${id}/users`,
    method: 'get'
  })
}
