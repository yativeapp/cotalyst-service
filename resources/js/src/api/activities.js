import request from '@/utils/request'

export function fetchList() {
  return request({
    url: '/activities',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/activities',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/activities',
    method: 'delete',
    data
  })
}


export function assign(providerId, activities) {

  return request({
    url: '/activities/assign',
    method: 'post',
    data: {
      providerId,
      activities
    }
  })
}
