import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url: '/schedules',
    method: 'get',
    params
  })
}

export function overmonths() {
  return request({
    url: '/schedules/overmonths',
    method: 'get'
  })
}

export function total() {
  return request({
    url: '/schedules/total',
    method: 'get'
  })
}
