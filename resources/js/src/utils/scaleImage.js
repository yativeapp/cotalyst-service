export function scaleImage(url, width, height) {
  var img = new Image();


  return new Promise((resolve, reject) => {
    img.onload = function () {
      var canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d");

      canvas.width = width;
      canvas.height = height;

      // draw the img into canvas
      ctx.drawImage(this, 0, 0, width, height);

      resolve(canvas.toDataURL());
    };

    img.src = url;
  });

}
