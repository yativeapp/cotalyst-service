import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [{
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    children: [{
      path: 'dashboard',
      meta: {
        title: 'Dashboard',
        icon: 'dashboard'
      },
      component: () => import('@/views/dashboard/index')
    }]
  },



  {
    path: '/users',
    component: Layout,
    children: [{
      path: '',
      name: 'Users',
      component: () => import('@/views/users/index'),
      meta: {
        title: 'Users',
        icon: 'peoples'
      }
    }, {
      path: ':id',
      name: 'UserDetail',
      component: () => import('@/views/users/detail'),
      hidden: true
    }]
  },
  {
    path: '/providers',
    component: Layout,
    children: [{
      path: '',
      name: 'Providers',
      component: () => import('@/views/providers/index'),
      meta: {
        title: 'Providers',
        icon: 'peoples'
      }
    }, {
      path: ':id',
      name: 'ProviderDetail',
      component: () => import('@/views/providers/detail'),
      hidden: true
    }]
  },
  {
    path: '/communities',
    component: Layout,
    children: [{
      path: '',
      name: 'Communities',
      component: () => import('@/views/communities/index'),
      meta: {
        title: 'Communities',
        icon: 'peoples'
      }
    }]
  },
  {
    path: '/activities',
    component: Layout,
    children: [{
      path: '',
      name: 'Activities',
      component: () => import('@/views/activities/index'),
      meta: {
        title: 'Activities',
        icon: 'form'
      }
    }]
  },
  {
    path: '/governorates',
    component: Layout,
    children: [{
      path: '',
      name: 'Governorates',
      component: () => import('@/views/governorates/index'),
      meta: {
        title: 'Governorates',
        icon: 'international'
      }
    }]
  },
  {
    path: '/challenges',
    component: Layout,
    children: [{
      path: '',
      name: 'Challenges',
      component: () => import('@/views/challenges/index'),
      meta: {
        title: 'Challenges',
        icon: 'example'
      }
    }]
  },
  {
    path: '/courts',
    component: Layout,
    children: [{
      path: '',
      name: 'Courts',
      component: () => import('@/views/courts/index'),
      meta: {
        title: 'Courts',
        icon: 'component'
      }
    }]
  },
  {
    path: '/transactions',
    component: Layout,
    children: [{
      path: '',
      name: 'Transactions',
      component: () => import('@/views/transactions/index'),
      meta: {
        title: 'Transactions',
        icon: 'money'
      }
    }]
  },
  {
    path: '/inquries',
    component: Layout,
    children: [{
      path: '',
      name: 'Inquries',
      component: () => import('@/views/inquries/index'),
      meta: {
        title: 'Inquries',
        icon: 'message'
      }
    }]
  },



  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

export default new Router({
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})
