<!DOCTYPE html>
<html>

     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Catalysty - admin</title>
          <!-- Bulma Version 0.7.2-->
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css" />
     </head>

     <body>
          <section class="hero is-black is-bold is-fullheight">
               <div class="hero-body">
                    <div class="container has-text-centered">
                         <div class="column is-4 is-offset-4">
                              <h3 class="title has-text-white">Login</h3>
                              <p class="subtitle has-text-grey">Please login to proceed.</p>
                              <div class="box" id="app">

                                   <Login></Login>
                              </div>

                         </div>
                    </div>
               </div>
          </section>

          <script src="/js/login.js"></script>
     </body>

</html>
