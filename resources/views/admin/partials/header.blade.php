<!-- Hero head: will stick at the top -->
<div class="hero-head">
     <header class="navbar">
          <div class="container">
               <div class="navbar-brand">
                    <a class="navbar-item">
                         CATALAYSTY
                    </a>
                    <span class="navbar-burger burger" data-target="navbarMenuHeroC">
                         <span></span>
                         <span></span>
                         <span></span>
                    </span>
               </div>
               <div id="navbarMenuHeroC" class="navbar-menu">
                    <div class="navbar-end">
                         <span class="navbar-item">
                              <a href="/" class="button is-danger is-inverted">
                                   <span>Logout</span>
                              </a>
                         </span>
                    </div>
               </div>
          </div>
     </header>
</div>
