<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get('/', 'HomeController@index');

$router->post('/admin/login', 'HomeController@login');

$router->post('/user/login', 'AuthController@login');
$router->post('/user/fb/login', 'AuthController@fbLogin');
$router->post('/user/google/login', 'AuthController@googleLogin');
$router->post('/user/signup', 'AuthController@signup');
$router->post('/provider/login', 'AuthController@providerLogin');
$router->get('/dashboard', 'HomeController@dashboard');
$router->delete('/fcm/token', 'AuthController@deleteToken');
