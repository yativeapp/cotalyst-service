<?php 

date_default_timezone_set('Africa/Cairo');

$router->patch('/fcm/token', 'AuthController@updateToken');
$router->patch('/user/avatar/update', 'AuthController@updateAvatar');


$router->get('/user/get/{id}', 'UsersController@show');
$router->get('/user', 'UsersController@user');
$router->get('/users', 'UsersController@index');
$router->get('/users/total', 'UsersController@total');
$router->get('/users/overmonths', 'UsersController@overmonths');

$router->patch('/user/change/username', 'SettingsController@username');
$router->patch('/user/change/password', 'SettingsController@password');
$router->patch('/user/change/number', 'SettingsController@number');

$router->get('/activities', 'ActivityController@index');

$router->get('/activities/{providerId}/show', 'ActivityController@showForProvider');
$router->get('/provider/activities/show', 'ActivityController@showForProvider');

$router->post('/activities', 'ActivityController@store');
$router->delete('/activities', 'ActivityController@destroy');

$router->post('/activities/assign', 'ActivityController@assign');


$router->get('providers', 'ProvidersController@index');
$router->get('/providers/search', 'ProvidersController@search');
$router->get('/providers/search/name', 'ProvidersController@searchByName');
$router->get('/providers/overmonths', 'ProvidersController@overmonths');
$router->get('/providers/total', 'ProvidersController@total');
$router->get('providers/{id}', 'ProvidersController@show');
$router->get('providers/{id}/activities', 'ProvidersController@activites');

$router->post('providers', 'ProvidersController@store');


$router->get('/courts', 'CourtsController@index');
$router->get('/courts/provider/{providerId}/activity/{activityId}/show', 'CourtsController@byActivity');
$router->get('/courts/{courtId}/show', 'CourtsController@show');
$router->post('/courts/store', 'CourtsController@store');
$router->patch('/courts/approve', 'CourtsController@approve');
$router->patch('/courts/decline', 'CourtsController@decline');
$router->patch('/courts/remove', 'CourtsController@remove');
$router->post('/courts/admin/create', 'CourtsController@immediateStore');

$router->post('/schedules/{courtId}/show', 'SchedulerController@show');
$router->post('/schedules/provider/{courtId}/show', 'SchedulerController@showForProvider');
$router->post('/courts/{courtId}/schedule', 'SchedulerController@store');

$router->get('/schedules/active', 'SchedulerController@active');
$router->get('/schedules/total', 'SchedulerController@total');
$router->get('/schedules/past', 'SchedulerController@past');
$router->get('/schedules', 'SchedulerController@index');
$router->delete('/schedules', 'SchedulerController@destroy');
$router->delete('/provider/schedules', 'SchedulerController@destroyUserReservation');
$router->post('/schedules/checkout', 'SchedulerController@store');
$router->post('/schedule/provider/manaual', 'SchedulerController@manual');


$router->get('/charts/gender', 'ChartsController@gender');
$router->get('/charts/months', 'ChartsController@months');
$router->get('/charts/providers/users', 'ChartsController@providersUsers');

$router->get('/governorates', 'GovernorateController@index');
$router->post('/governorates', 'GovernorateController@store');
$router->delete('/governorates', 'GovernorateController@destroy');

$router->get('/challenges', 'ChallengeController@index');
$router->get('/challenges/total', 'ChallengeController@total');
$router->get('/user/challenges', 'ChallengeController@user');
$router->get('/user/challenges/active', 'ChallengeController@getActive');
$router->get('/user/challenges/requests', 'ChallengeController@getRequests');
$router->get('/user/challenges/history', 'ChallengeController@getHistory');
$router->get('/challenge/{id}', 'ChallengeController@show');
$router->get('/challenge/{id}/schedules', 'ChallengeController@schedules');
$router->get('/challenge/{id}/matches', 'ChallengeController@matches');
$router->post('/challenge/store', 'ChallengeController@store');
$router->post('/challenge/accept', 'ChallengeController@accept');

//requests
$router->post('/challenge/request', 'RequestsController@store');
$router->post('/challenge/request/accept', 'RequestsController@accept');
$router->post('/challenge/request/cancel', 'RequestsController@cancel');
$router->post('/challenge/request/decline', 'RequestsController@decline');
//
$router->patch('/challenge/timestamp/change', 'ChallengeController@changeTimeStamp');
$router->patch('/challenge/name/change', 'ChallengeController@changeName');
$router->get('/users/search', 'ProvidersController@searchUser');


$router->post('chat/notify', 'ChatController@notify');


$router->post('/challenge/{challengeid}/match', 'MatchController@store');
$router->post('/match/cancel', 'MatchController@destroy');
$router->get('/match/{id}', 'MatchController@show');


$router->get('communities', 'CommunityController@index');
$router->get('/user/communities', 'CommunityController@user');
$router->post('/community/store', 'CommunityController@store');
$router->get('/community/search', 'CommunityController@search');
$router->get('/community/{id}', 'CommunityController@show');
$router->get('/community/{id}/users', 'CommunityController@getUsers');
$router->get('/community/{id}/challenges', 'CommunityController@challenges');
$router->post('/community/user/remove', 'CommunityController@removeUser');
$router->post('/community/user/join', 'CommunityController@joinUser');
$router->post('/community/user/accept', 'CommunityController@acceptUser');


$router->get('/reviews', 'ReviewController@index');
$router->post('/reviews/store', 'ReviewController@store');


$router->get('user/invites', 'InviteController@index');
$router->get('invites/user/search', 'InviteController@search');
$router->post('community/invite', 'InviteController@store');
$router->post('invite/accept', 'InviteController@accept');
$router->post('invite/decline', 'InviteController@decline');



$router->post('challenge/score/change', 'ScoreController@store');



$router->get('inquries', 'InquryController@show');
$router->get('inquries/all', 'InquryController@index');
$router->post('inquries', 'InquryController@store');
$router->post('inquries/resolve', 'InquryController@resolve');


$router->get('schedules/overmonths', 'SchedulerController@overmonths');

