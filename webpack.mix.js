let mix = require('laravel-mix');


mix.js('resources/js/app.js', 'js/app.js')
  .js('resources/js/login.js', 'js/login.js')
  .sass('resources/sass/app.sass', 'css/app.css');